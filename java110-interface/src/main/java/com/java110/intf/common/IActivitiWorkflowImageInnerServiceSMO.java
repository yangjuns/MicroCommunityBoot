package com.java110.intf.common;

public interface IActivitiWorkflowImageInnerServiceSMO {

    /**
     * 启动流程
     *
     * @return
     */
    public String getWorkflowImage(String taskId);


}
